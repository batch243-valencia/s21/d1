// console.info("hello");

// An Array in programming  is simple a list of DATA
// Data that are related or connected w/ each other

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

// with an array, we can simply write the code above like this;
let studentNumbers = [
    "2020-1923",
    "2020-1924",
    "2020-1925",
    "2020-1926",
    "2020-1927"];
    
    // [Section] Arrays

    // -arrays are used to store multiple related values in single variable.
    // -they are declared using square brackets ([]) also know as "Array Literals".
    // -array  also  provide access a number  of function/methods that help achieving specific task.

    // method is another term for functions associated with an object/array and is used to execute statements that are relevant
    // majority of methods are used to manipulate information store within the same object.

    // Syntax
        // let/const  arrayName = [elementA, elementB, elementC]
    let grades= [98.5,94.3,89.2,90.1];
    console.log(grades);
    let computerBrands = ["Acer", "Asus", "Lenovo", "Dell", "Mac", "Samsung"];
    console.log(computerBrands);
    // 
    let mixedArr= [12,'Asus', null, undefined, {} ];
    console.log(mixedArr);

    let myTasks = [
        "drink html",
        "eat javascript",
        "inhale css",
        "bake sass"];
        console.log(myTasks);

    let city1 = "Tokyo";
    let city2 = "Manila";
    let city3 = "New York";
    

    let cities = [city1,city2,city3];
    console.log(cities);

    // [Section] length property
    // the .length property allows us to get and set the total number of items.
    console.log(grades);
    grades.length=2
    console.log(grades.length);//2
    console.log(grades);

    let blankArr= [];;
    console.log(blankArr.length);//(2) [98.5, 94.3]

    let fullName= "John Doe";
    console.log(fullName.length);//8
    console.log(myTasks[1].length);//14

    // 
    myTasks.length--;//['drink html', 'eat javascript', 'inhale css', 'bake sass']
    console.log(myTasks);//(3) ['drink html', 'eat javascript', 'inhale css']
    myTasks.length-=1;
    console.log(myTasks);//(3) ['drink html', 'eat javascript']
    myTasks.length=myTasks.length-1;
    console.log(myTasks);//(3) ['drink html']

    // To delete a specific items in array we can employ array 
    cities.length--;

    // we cant change STRING's length
    console.log(fullName);//John Doe
    fullName.length--;
    console.log(fullName);//John Doe

    let theBeatles = ["John","Paul", "Ringo", "George"]
    theBeatles.length++;
    console.log(theBeatles)
    theBeatles[theBeatles.length-1]="Roda";
    
    // [Section] Reading/Accession elements of arrays
        // Accessing array elements is one of the more common task that we do with an array
        // this can be done through the use of index.
        // Each element in an array is associated with it's own index.

    let lakersLegends=["Kobe","Shaq", "Lebron", "Magic", "Kareem"];
    console.log(lakersLegends[1]);//Shaq
    // We can also save/store array elements in another variable;
    let currectLaker = lakersLegends[2];
    console.log(currectLaker);

    // We can also reassign array values using items indices.

    console.log ("Array before the reassignment:");
    console.log (lakersLegends);
    lakersLegends[2]= "Pau Gasol";
    console.log("Array after the reassignment:");
    console.log(lakersLegends); 

    // Accessing the last element of an array
    // Since the firest element of an array start at 0
    // subtracting 1 to the elements of an array will offset the value
    // by one allowing us to get the last elements.

    let lastElementIndex= lakersLegends.length-1; 
    console.log(lakersLegends[lastElementIndex]);

    // adding items into the array w/o using array methods
    let newArr=[];
    newArr[0]="Cloud Strife";
    console.log(newArr);
    newArr[newArr.length]="Tifa Lockhart";
    console.log(newArr);

    // looping over an array
    
    for (let index = 0; index<newArr.length;index++){
        console.log(newArr[index]);
    }

    let numArr = [5,12,30,46,40];
    for (let index = 0; index<numArr.length;index++){
        if (numArr[index]%5===0){
            console.log("NumArr is Divisible by 5:"+numArr[index]);
        }else{
            console.log(numArr[index]+" is not divisible by 5. the remainder is:"+numArr[index]%5);
        }
    }
    
    // [Section] MultiDimensional Arrays
        // multidimensional arrays are useful for storing complex data structures.
        // a practical application of this is to help visualize create real world objects
    
    let chessBoard = [
        ['a1', 'b1','c1','d1','e1','f1','g1','h1'],
        ['a2', 'b2','c2','d2','e2','f2','g2','h2'],
        ['a3', 'b3','c3','d3','e3','f3','g3','h3'],
        ['a4', 'b4','c4','d4','e4','f4','g4','h4'],
        ['a5', 'b5','c5','d5','e5','f5','g5','h5'],
        ['a6', 'b6','c6','d6','e6','f6','g6','h6'],
        ['a7', 'b7','c7','d7','e7','f7','g7','h7'],
        ['a8', 'b8','c8','d8','e8','f8','g8','h8'],
    ]

    console.log(chessBoard);
